function defaults(obj,defaultprops){
   if(obj==""){
    return defaultprops;
   }
   if(defaultprops==""){
    return obj;
   }
  
   
   return {...obj,...defaultprops}
}
module.exports = defaults;